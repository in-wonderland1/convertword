def extract_words(filename: str, given_word: str) -> list[str]:
    def relevent_words(given_word: str, chosen: str) -> bool:
        return len(given_word) == len(chosen)
    file = open(filename)
    return [word.strip() for word in file if relevent_words(word.strip(), given_word)]


def word_differs_by_a_letter(choice: str, word: str) -> bool:
    return len(set(choice) & set(word)) == 3

def filter_data(data: list[str], given_word: str) -> list[str]:
    return [word for word in data if word_differs_by_a_letter(word, given_word)] 

print(filter_data(extract_words('sowpods.txt', 'CLAY'), 'CLAY'))

def load_data(words_file: str) -> list[tuple]:
    with open(words_file) as f:
        words = [line.strip() for line in f ]
    return words

def convert ( word1 : str ,word2 : str ,words : list[str]) -> list[str] :
    possible = [word1]
    word2 = list(word2)
    for word in possible :
        i = 0
        dup = list(word)
        for pos,(l1,l2) in enumerate(zip(dup,word2)):
            dup = list(word)
            if l1 != l2 :
                dup[pos] = l2
                dup = "".join(dup)
                if dup in words:
                    possible.append(dup)
                    i += 1
        if i == 0:
            for word in words :
                if dup[1:] == word[1:]:
                    possible.append(word)

    return set(possible)

def final( word1 ,word2 ) :
    words = [ word for word in load_data("sowpods.txt") if len(word) == len(word1)]
    print(convert(word1,word2,words))

#print(convert("clay","gold",["clad","glad","goad","gold"]))
final("PLAY","GOLD")





